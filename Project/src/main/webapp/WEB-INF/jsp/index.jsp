<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン画面</title>
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->

</head>
<body>

	<!-- header -->
	<header>

		<h1>ログイン画面</h1>

	</header>
	<!-- /header -->

	<!-- body -->


		<c:if test="${errMsg != null}">
			<p class="alert alert-danger" role="alert">${errMsg}</p>

		</c:if>

		<div class="card card-container">
			<p id="profile-name" class="profile-name-card"></p>
			<form class="form-signin" action="LoginServlet" method="post">
				<span id="reauth-email" class="reauth-email"></span>
				<div class="col-sm-6">
				ログインID <input type="text" style="width:200px;"  name="loginId" id="inputLoginId" class="form-control"
					placeholder="ログインID" required autofocus><br>
				パスワード <input type="password" style="width:200px;" name="password" id="inputPassword"
					class="form-control" placeholder="パスワード" required><br>
					
				 <button>ログイン</button>
				 </div>
		</form>
		<!-- /form -->
	</div>
	<!-- /card-container -->
	
</body>
</html>

