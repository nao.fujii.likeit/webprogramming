<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ更新画面</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">

</head>

<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
			<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
				<li class="nav-item active"></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link" href="LoginServlet">${userInfo.name}さん</a></li>
				<li class="nav-item"><a class="btn btn-outline-danger"
					href="LogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	
		<div class="container">
		<h1>ユーザ情報更新</h1>
	
	<!-- /header -->

	<!-- body -->

		<c:if test="${errMsg != null}">
			<p class="alert alert-danger" role="alert">${errMsg}</p>
		</c:if>

		<form method="post" action="#" class="form-horizontal">
			<div class="form-group row">
				<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<p class="form-control-plaintext">${User.loginId}</p>
					<input type="hidden" name="loginId" class="form-control"
						id="loginId" value=${User.loginId}>
				</div>
			</div>

			<div class="form-group row">
				<label for="password" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input type="password" name="password" class="form-control"
						id="password">
				</div>
			</div>

			<div class="form-group row">
				<label for="passwordConf" class="col-sm-2 col-form-label">パスワード(確認)</label>
				<div class="col-sm-10">
					<input type="password" name="passwordConf" class="form-control"
						id="passwordConf">
				</div>
			</div>

			<div class="form-group row">
				<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
				<div class="col-sm-10">
					<input type="text" name="userName" class="form-control"
						id="userName" value=${User.name}>
				</div>
			</div>

			<div class="form-group row">
				<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="date" name="birthDate" class="form-control"
						id="birthDate" value=${User.birthDate}>
				</div>
			</div>

			<div class="submit-button-area">
				<button type="submit" value="検索"
					class="btn btn-secondary btn-lg btn-block">更新</button>
			</div>

			<div class="col-xs-4">
				<a href="UserListServlet">戻る</a>
			</div>

		</form>


	</div>
	</div>
</body>

</html>