<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ログイン画面</title>
    <!-- BootstrapのCSS読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/login.css" rel="stylesheet">
</head>

<body>

    <!-- body -->
    <form class="form-signin" action="userList.html">
        <div class="text-center mb-4">
            <h1>ログイン画面</h1>
        </div>

        <div class="form-label-group">
            ログインID　<input type="text" id="inputLoginId" class="form-control" placeholder="ログインID" autofocus>
            <label for="inputLoginId"></label>
        </div>

        <div class="form-label-group">
            Password　<input type="password" id="inputPassword" class="form-control" placeholder="Password">
            <label for="inputPassword"></label>
        </div>

        <button>ログイン</button>
    </form>


</body>

</html>