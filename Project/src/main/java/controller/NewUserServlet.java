package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

@WebServlet("/NewUserServlet")
public class NewUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public NewUserServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		
		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");
		
		
		//パスワードが確認用と不一致の時
		if (!(password.equals(passwordConf))){
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg","入力された内容は正しくありません。");
			System.out.println("パスワードが合っていない");
			//新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		/** 入力漏れがあった場合 **/
		if (loginId.equals("") || password.equals("") || passwordConf.equals("")  || userName.equals("")  || birthDate.equals("") ) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg","入力された内容は正しくありません。");
			System.out.println("入力漏れがある");
			// 新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		/** 既に登録されているログインIDだった場合 **/
		UserDao loginIdDao = new UserDao();
		
		if (loginIdDao.findByLoginId(loginId) != null ){
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg","入力された内容は正しくありません。");
			System.out.println("ログインID被ってる");
			// 新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		/** パスワードの暗号化 **/
		UserDao PasslockDao = new UserDao();
		String result = PasslockDao.PassMD5(password) ;
		
		UserDao InsertuserDao = new UserDao();
		InsertuserDao.InsertUser(0, loginId, userName, birthDate, result);
		
		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");
		
	}
}