package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UpdateServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// idを引数にして、idに紐づくユーザ情報を出力する
		User user = UserDao.findByUpdate(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("User", user);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");

		/** パスワードが確認用と不一致の時 **/
		if (!(password.equals(passwordConf))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			System.out.println("パスワードが合っていない");
			//更新jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** 入力漏れがあった場合 **/
		if (loginId.equals("") || userName.equals("") || birthDate.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			System.out.println("入力漏れがある");
			// 更新jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		/** パスワードの暗号化 **/
		UserDao PasslockDao = new UserDao();
		String result = PasslockDao.PassMD5(password);

		/** パスワード空欄の場合そのままパスワード以外を更新する **/
		if (password.equals("") && passwordConf.equals("")) {
			// パスワードなしで更新
			UserDao NopassUpdateDao = new UserDao();
			NopassUpdateDao.UpdateUserNoPass(loginId, userName, birthDate);
		} else {
			/** それ以外の場合の更新作業 **/
			UserDao UpdateDao = new UserDao();
			UpdateDao.UpdateUser(loginId, userName, birthDate, result);
		}

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");
	}
}