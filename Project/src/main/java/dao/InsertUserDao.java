package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertUserDao {

	public InsertUserDao(int id, String loginId, String name, String birthDate, String password) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, password);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データの登録に成功しました。」と表示する
			// 登録に失敗した場合、「データの登録に失敗しました。」と表示する
			if (result > 0) {
			} else {
				System.out.println("入力された内容は正しくありません");
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("入力された内容は正しくありません");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
