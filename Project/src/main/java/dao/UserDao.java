package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 管理者以外の全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE login_id != 'admin' ";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * 新規ユーザ情報を登録する
	 * @return
	 */
	public void InsertUser(int id, String loginId, String name, String birthDate, String password) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, password);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データの登録に成功しました。」と表示する
			// 登録に失敗した場合、「データの登録に失敗しました。」と表示する

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("データの登録に成功しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * IDが紐づくユーザ情報を返す
	 * @param Id
	 * @return
	 */
	public static User findById(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータ全てインスタンスのフィールドに追加
			int idDate = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(idDate, loginId, name, birthDate, password, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * ユーザ情報を更新する
	 * @param name
	 * @param password
	 * @param birth_date
	 * @return
	 */

	public static User findByUpdate(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータ全てインスタンスのフィールドに追加
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String updateDate = rs.getString("update_date");
			return new User(loginId, name, birthDate, password, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void UpdateUser(String loginId, String name, String birth_date, String password) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "UPDATE user SET name = ? ,birth_date = ? ,password = ? ,update_date = now() WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, name);
			pStmt.setString(2, birth_date);
			pStmt.setString(3, password);
			pStmt.setString(4, loginId);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データの登録に成功しました。」と表示する
			// 登録に失敗した場合、「データの登録に失敗しました。」と表示する

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("データの更新に成功しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * パスワードは変更なしでユーザ情報を更新する
	 * @param name
	 * @param birth_date
	 * @return
	 */
	public void UpdateUserNoPass(String loginId, String name, String birth_date) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "UPDATE user SET name = ? ,birth_date = ? ,update_date = now() WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, name);
			pStmt.setString(2, birth_date);
			pStmt.setString(3, loginId);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データの登録に成功しました。」と表示する
			// 登録に失敗した場合、「データの登録に失敗しました。」と表示する

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("入力された内容は正しくありません");

		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("パスワードは更新せず他の項目のみ更新しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * ユーザ情報を削除する
	 * @return
	 */

	public static User findByDelete(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータ全てインスタンスのフィールドに追加
			String loginId = rs.getString("login_id");
			return new User(loginId);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void UserDelete(String loginId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String insertSQL = "DELETE FROM user WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			pStmt.setString(1, loginId);

			//*実行するための文章
			int result = pStmt.executeUpdate();

			// 登録に成功した場合、「データ削除しました。」と表示する
			// 登録に失敗した場合、「データの削除に失敗しました。」と表示する

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("データの削除に失敗しました。");
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					System.out.println("データ削除しました。");
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * ログインIDの被りを確認する
	 * @param loginId
	 * @return
	 */

	public User findByLoginId(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT login_id FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			System.out.println(loginIdData);
			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * パスワードを暗号化する
	 * @param password
	 * @return MD5
	 */
	public String PassMD5(String password) {

		//ハッシュを生成したい元の文字列
		String source = password;

		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;

		//ハッシュアルゴリズム
		String algorithm = "MD5";
		String result = "";
		try {
			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

			result = DatatypeConverter.printHexBinary(bytes);

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 検索機能 ユーザ情報を取得する
	 * ログインID（完全一致）
	 * ユーザ名（部分一致）
	 * 生年月日（開始日と終了日の範囲内に該当するもの）
	 * @return
	 */
	public List<User> findDate(String loginId, String userName, String dateStart, String dateEnd) {
		Connection conn = null;
		List<User> findList = new ArrayList<User>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id != 'admin'";
			if (!loginId.equals("")) {
				sql += "AND login_id = '" + loginId + "'";
			}
			if (!userName.equals("")) {
				sql += "AND name LIKE '" + "%" + userName + "%" + "'";
			}
			if (!dateStart.equals("")) {
				sql += "AND birth_date >=  '" + dateStart + "'";
			}
			if (!dateEnd.equals("")) {
				sql += "AND birth_date <  '" + dateEnd + "'";
			}
			
			// SELECTを実行し、結果表を取得
			Statement Stmt = conn.prepareStatement(sql);
			ResultSet rs = Stmt.executeQuery(sql);
			
			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String LoginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");                                                                                                                                                                                                               
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, LoginId, name, birthDate, password, createDate, updateDate);

				findList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return findList;
	}
}
